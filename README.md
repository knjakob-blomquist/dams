# Data analysis made simple with python

This jupyter notebook goes through how to use python in you data analysis. Mainly analysis of experiments, such as peak analysis and curve fitting.

The easiest way to get started is to download all files either using
git:

git clone https://gitlab.com/knjakob-blomquist/dams.git

or directly from GitLab project page. 

After this is done, if you don't have jupyter installed, start by
opening the non-interactive html-page of the notebook.

## Packages and installation
To be able to run all the code in this notebook you will need to have the following things installed:
- Python (Python 3 is preferred)
- ipython notebook (jupyter)
- matplotlib
- Numpy
- scipy
- pandas
- lmfit
- seaborn

The simplest way to install not only pandas, but Python and the most popular packages that make up the SciPy stack (IPython, NumPy, Matplotlib, …) is with [Anaconda](http://docs.continuum.io/anaconda/), a cross-platform (Linux, Mac OS X, Windows) Python distribution for data analytics and scientific computing. After running the installer, the user will have access to pandas and the rest of the SciPy stack without needing to install anything else, and without needing to wait for any software to be compiled.

First, download Anaconda. We recommend downloading [Anaconda’s](https://www.anaconda.com/downloads) latest Python 3 version.

Second, install the version of Anaconda which you downloaded, following the instructions on the download page.

Congratulations, you have installed Jupyter Notebook! To run the notebook, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows):
jupyter notebook
